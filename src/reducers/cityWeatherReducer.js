let initialState = {
    cityWeather: {},
    currentCityList: []
  };
  
  const cityWeatherReducer = (state = initialState, action) => {
    if (action.type === "LOAD_CITY_CURRENT_WEATHER") {
      return {
        cityWeather: action.cityWeather,
        currentCityList: action.currentCityList
      };
    } else {
      return state;
    }
  };
  
  export default cityWeatherReducer;
  