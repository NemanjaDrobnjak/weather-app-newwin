import { combineReducers } from "redux";
import cityWeatherReducer from "./cityWeatherReducer";
import citiesReducer from "./citiesReducer";
import searchReducer from "./searchReducer";

const rootReducer = combineReducers({
  cityWeather: cityWeatherReducer,
  citiesList: citiesReducer,
  visibleCities: searchReducer
});

export default rootReducer;
