let initialState = {
    visibleCities: []
  };
  
  const searchReducer = (state = initialState, action) => {
    if (action.type === "HANDLE_SEARCH_CHANGE") {
      return {
        visibleCities: action.citiesOnSearch
      };
    } else {
      return state;
    }
  };
  
  export default searchReducer;
  