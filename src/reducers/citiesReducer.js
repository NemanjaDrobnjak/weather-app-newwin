let initialState = {
  citiesList: [],
};

const citiesReducer = (state = initialState, action) => {
  if (action.type === "LOAD_ALL_CITIES") {
    return {
      citiesList: action.citiesList,
    };
  } else if (action.type === "HANDLE_SEARCH_CHANGE") {
    return {
      citiesList: action.citiesOnSearch,
    };
  } else {
    return state;
  }
};

export default citiesReducer;
