import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "./reducers/rootReducer";

export default function configureStore() {
  const middlewareEnhancer = applyMiddleware(thunk);

  const store = createStore(rootReducer, middlewareEnhancer);

  return store;
}
