import axios from "axios";

import { extractJsonData } from "../helpers/helpers";

export const currentWeatherForCity = (cityId) => {
  return function (dispatch, getState) {
    let currentCityList = getState().cityWeather.currentCityList;

    axios
      .get(
        `http://api.openweathermap.org/data/2.5/weather?id=` +
          cityId +
          `&appid=ecbc293668d0be3b2c5c155b191e46ad&units=metric`
      )
      .then((res) => {
        let cityWeather = res.data;
        console.log("+++++", currentCityList);
        currentCityList.push(cityWeather);
        dispatch({
          type: "LOAD_CITY_CURRENT_WEATHER",
          cityWeather,
          currentCityList,
        });
      })
      .catch((err) => {
        console.log("ERROR ", err);
      });
  };
};

export const createNewCityArray = () => {
  return function (dispatch) {
    let citiesList = extractJsonData();
    console.log(citiesList);
    dispatch({ type: "LOAD_ALL_CITIES", citiesList });
  };
};

export const handleSearchChange = (event) => {
    console.log(event.target.value);
  return function (dispatch, getState) {
    let allCities = getState().citiesList.citiesList;
    let citiesOnSearch = allCities.filter((city) => {
      return city.cityName.toLowerCase().includes(event.target.value.toLowerCase());
    });

    if (event.target.value === "") {
        citiesOnSearch = allCities;
    }
    dispatch({ type: "HANDLE_SEARCH_CHANGE", citiesOnSearch });
  };
};
