import myData from './data.json';
import uuid from "uuid-random";

export const extractJsonData = () => {
    return myData.map(item => {
        return {
            cityName: item.name,
            cityId: item.id
        }
    });
}

export const getUniqueKey = () => {
    return uuid();
  }
  