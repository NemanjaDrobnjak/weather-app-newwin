import React from "react";

import { connect } from "react-redux";

import Header from "./Header";
import FilterBar from "./FilterBar";
import MainWidget from "./MainWidget";

import {
  currentWeatherForCity,
  createNewCityArray,
  handleSearchChange,
} from "../actions/index";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.createNewCityArray();
  }

  // next step add shouldComponentUpdate to re-render the component and update MainWidget props

  render() {
    const { cities, currentCityList } = this.props;
    return (
      <div>
        <Header />
        <FilterBar />
        <MainWidget
          cities={cities}
          handleSearchChange={this.props.handleSearchChange}
          currentWeatherForCity={this.props.currentWeatherForCity}
          currentCityList={currentCityList}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cityWeather: state.cityWeather.cityWeather,
    currentCityList: state.cityWeather.currentCityList,
    cities: state.citiesList.citiesList
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    currentWeatherForCity: (cityId) => dispatch(currentWeatherForCity(cityId)),
    createNewCityArray: () => dispatch(createNewCityArray()),
    handleSearchChange: (e) => dispatch(handleSearchChange(e)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
