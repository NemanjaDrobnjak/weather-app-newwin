import React from "react";

import { getUniqueKey } from "../helpers/helpers";

class MainWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.addCityToTheList = this.addCityToTheList.bind(this);
  }

  addCityToTheList(cityId) {
    this.props.currentWeatherForCity(cityId);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.props.cities !== nextProps.cities ||
      this.props.currentCityList !== nextProps.currentCityList
    );
  }

  render() {
    return (
      <div className="main__container">
        <div className="search__container">
          <div className="search__input">
            <input
              className="search__field"
              placeholder="Search for the city..."
              onChange={(event) => this.props.handleSearchChange(event)}
            ></input>
          </div>
          <div className="search__dropdown">
            {this.props.cities &&
              this.props.cities.map((city) => {
                return (
                  <div key={getUniqueKey()} className="search__row">
                    <div className="city__name">{city.cityName}</div>
                    <div
                      className="add__button"
                      onClick={() => {
                        this.addCityToTheList(city.cityId);
                      }}
                    >
                      Add
                    </div>
                  </div>
                );
              })}
          </div>
        </div>
        <div className="list__container">
          {this.props.currentCityList &&
            this.props.currentCityList.map((cityItem) => {
              return (
                <div key={getUniqueKey()} className="city__item-row">
                  <div className="city__name">{cityItem.name}</div>
                  <div className="city__name">{cityItem.main.temp}</div>
                </div>
              );
            })}
        </div>
      </div>
    );
  }
}

export default MainWidget;
